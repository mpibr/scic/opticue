#include <QApplication>
#include <QMetaType>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qRegisterMetaType<WId>("WId");
    MainWindow w;
    w.show();
    return a.exec();
}
