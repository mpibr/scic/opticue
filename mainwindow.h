#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QCloseEvent>
#include "framegrabber.h"
#include "frameviewer.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    FrameGrabber *m_grabber;
    FrameViewer *m_viewer;
    QThread *m_thread_grabber;

    void closeEvent(QCloseEvent *event);

    void setupThreads();
    void setupUiWidgets();

signals:
    void quitRequested();

};
#endif // MAINWINDOW_H
