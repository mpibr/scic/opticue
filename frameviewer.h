#ifndef FRAMEVIEWER_H
#define FRAMEVIEWER_H

#include <QWidget>

class FrameViewer : public QWidget
{
    Q_OBJECT

public:
    explicit FrameViewer(QWidget *parent = nullptr);
    ~FrameViewer();

    QSize sizeHint() const override { return QSize(1024, 1024); };

public slots:
    void on_resizeWindow(int windowWidth, int windowHeight);

protected:
    bool event(QEvent *event) override;
    void paintEvent(QPaintEvent *event) override;

signals:
    void windowHandleAssigned(WId widgetWindowId);
};

#endif // FRAMEVIEWER_H
