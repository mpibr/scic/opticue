#ifndef FRAMEGRABBER_H
#define FRAMEGRABBER_H

#include <QObject>
#include <QWidget>
#include <mil.h>

/* default frame buffer dimensions. */
#define DEFAULT_BUFFER_SIZE_X       1024
#define DEFAULT_BUFFER_SIZE_Y       1024
#define DEFAULT_BUFFER_SIZE_BAND      1
#define DEFAULT_BUFFER_TYPE 16+M_UNSIGNED

class FrameGrabber : public QObject
{
    Q_OBJECT
public:
    explicit FrameGrabber(QObject *parent = nullptr);
    ~FrameGrabber();

public slots:
    void on_assignWindowHandle(WId widgetWindowId);
    void on_grab();
    void on_capture();
    void on_stop();

private:
    bool m_isInitialized;
    bool m_isGrabbingContinous;
    MIL_ID m_application;
    MIL_ID m_system;
    MIL_ID m_display;
    MIL_ID m_digitizer;
    MIL_ID m_buffer;
    MIL_INT m_bufferSizeX;
    MIL_INT m_bufferSizeY;
    MIL_INT m_bufferSizeBand;
    MIL_INT m_bufferType;

    void deallocate_resources();

signals:
    void windowResized(int windowWidth, int windowHeight);
};

#endif // FRAMEGRABBER_H
