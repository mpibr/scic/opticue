#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_grabber(new FrameGrabber())
    , m_viewer(new FrameViewer(this))
    , m_thread_grabber(new QThread(this))
{
    ui->setupUi(this);
    setupThreads();
    setupUiWidgets();
}


MainWindow::~MainWindow()
{
    if (m_thread_grabber) {
        m_thread_grabber->quit();
        m_thread_grabber->wait();
    }
    delete ui;
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    event->ignore();
    if (QMessageBox::Yes == QMessageBox::question(this, "Close confirmation", "Exit?", QMessageBox::Yes | QMessageBox::No)) {

        // clean export path
        emit quitRequested();
        event->accept();
    }
}



void MainWindow::setupThreads()
{
    m_grabber->moveToThread(m_thread_grabber);
    connect(m_thread_grabber, &QThread::finished, m_grabber, &FrameGrabber::deleteLater);
    m_thread_grabber->start();
}


void MainWindow::setupUiWidgets()
{
    // add buttons
    QPushButton *ui_pushButton_grab = new QPushButton("Grab", this);
    QPushButton *ui_pushButton_capture = new QPushButton("Capture", this);
    QPushButton *ui_pushButton_stop = new QPushButton("Stop", this);

    // connect buttons and main window slots
    connect(ui_pushButton_grab, &QPushButton::clicked, m_grabber, &FrameGrabber::on_grab);
    connect(ui_pushButton_capture, &QPushButton::clicked, m_grabber, &FrameGrabber::on_capture);
    connect(ui_pushButton_stop, &QPushButton::clicked, m_grabber, &FrameGrabber::on_stop);
    connect(this, &MainWindow::quitRequested, m_grabber, &FrameGrabber::on_stop);

    QHBoxLayout *ui_layoutButtons = new QHBoxLayout;
    ui_layoutButtons->addWidget(ui_pushButton_grab);
    ui_layoutButtons->addWidget(ui_pushButton_capture);
    ui_layoutButtons->addWidget(ui_pushButton_stop);

    QVBoxLayout *ui_layoutMain = new QVBoxLayout;
    ui_layoutMain->addLayout(ui_layoutButtons);
    ui_layoutMain->addWidget(m_viewer);

    connect(m_viewer, &FrameViewer::windowHandleAssigned, m_grabber, &FrameGrabber::on_assignWindowHandle);
    connect(m_grabber, &FrameGrabber::windowResized, m_viewer, &FrameViewer::on_resizeWindow);

    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(ui_layoutMain);
    setCentralWidget(centralWidget);
}
