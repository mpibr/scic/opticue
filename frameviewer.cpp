#include "frameviewer.h"
#include <QDebug>
#include <QMessageBox>
#include <QCloseEvent>

FrameViewer::FrameViewer(QWidget *parent)
    : QWidget(parent)
{
    setAttribute(Qt::WA_PaintOnScreen, true);
    setAttribute(Qt::WA_OpaquePaintEvent, true);
    setAttribute(Qt::WA_NoSystemBackground, true);
}


FrameViewer::~FrameViewer()
{

}


bool FrameViewer::event(QEvent *event)
{
    if (event->type() == QEvent::Show)
    {
        WId widgetWindowId = winId();
        emit windowHandleAssigned(widgetWindowId);
    }
    return QWidget::event(event);
}


void FrameViewer::paintEvent(QPaintEvent *event)
{

}


void FrameViewer::on_resizeWindow(int windowWidth, int windowHeight)
{
    setMinimumSize(windowWidth, windowHeight);
    resize(windowWidth, windowHeight);
    update();
    qDebug() << "FrameViewer resized to:" << windowWidth << "x" << windowHeight;
}
