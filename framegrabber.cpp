#include "framegrabber.h"
#include <QDebug>
#include <QString>

FrameGrabber::FrameGrabber(QObject *parent)
    : QObject{parent}
    , m_isInitialized(false)
    , m_isGrabbingContinous(false)
    , m_digitizer(M_NULL)
    , m_buffer(M_NULL)
    , m_bufferSizeX(DEFAULT_BUFFER_SIZE_X)
    , m_bufferSizeY(DEFAULT_BUFFER_SIZE_Y)
    , m_bufferSizeBand(DEFAULT_BUFFER_SIZE_BAND)
    , m_bufferType(DEFAULT_BUFFER_TYPE)
{
    /* allocate a MIL application */
    if (MappAlloc(M_NULL, M_DEFAULT, &m_application) == M_NULL) {
        qDebug() << "Error, failed to allocate MIL application.";
        deallocate_resources();
        return;
    }

    /* allocate a MIL system */
    if (MsysAlloc(M_DEFAULT, MIL_TEXT("M_DEFAULT"), M_DEFAULT, M_DEFAULT, &m_system) == M_NULL) {
        qDebug() << "Error, failed to allocate MIL system.";
        deallocate_resources();
        return;
    }

    /* allocate a MIL display */
    if (MdispAlloc(m_system, M_DEFAULT, MIL_TEXT("M_DEFAULT"), M_WINDOWED, &m_display) == M_NULL) {
        qDebug() << "Error, failed to allocate MIL display.";
        deallocate_resources();
        return;
    }

    /* allocate digitizer */
    MIL_STRING dcfFilePath = MIL_TEXT("config/acA2040-180km-24916686_Radient-eV-CL_12Bit-4Tab-2xBinning.dcf");
    if (MdigAlloc(m_system, M_DEFAULT, MIL_TEXT(dcfFilePath), M_DEFAULT, &m_digitizer) == M_NULL) {
        qDebug() << "Error, failed to allocate MIL digitizer.";
        deallocate_resources();
        return;
    }

    if (MsysInquire(m_system, M_DIGITIZER_NUM, M_NULL) == 0) {
        qDebug() << "Error, system does not find digitizer.";
        deallocate_resources();
        return;
    }


    /* allocate buffer */
    if (MbufAllocColor(m_system, m_bufferSizeBand, m_bufferSizeX, m_bufferSizeY, m_bufferType,
                       (m_digitizer ? M_IMAGE+M_DISP+M_GRAB : M_IMAGE+M_DISP), &m_buffer) == M_NULL) {
        qDebug() << "Error, failed to allocate MIL acquisition buffer.";
        deallocate_resources();
        return;
    }
    MbufClear(m_buffer, 0);

    m_isInitialized = true;
    qDebug() << "FrameGrabber is properly initialized.";
}


FrameGrabber::~FrameGrabber()
{
    deallocate_resources();
}


void FrameGrabber::deallocate_resources()
{
    if (m_buffer) {
        MbufFree(m_buffer);
        m_buffer = M_NULL;
    }

    if (m_digitizer) {
        MdigFree(m_digitizer);
        m_digitizer = M_NULL;
    }

    if (m_display) {
        MdispSelect(m_display, M_NULL);
        MdispFree(m_display);
        m_display = M_NULL;
    }

    if (m_system) {
        MsysFree(m_system);
        m_system = M_NULL;
    }

    if (m_application) {
        MappFree(m_application);
        m_application = M_NULL;
    }

    m_isInitialized = false;
}


void FrameGrabber::on_assignWindowHandle(WId widgetWindowId)
{
    if (!m_isInitialized) {
        qDebug() << "Error, frame grabber not initialized!";
        return;
    }
    MIL_WINDOW_HANDLE widgetWindowHandle = reinterpret_cast<MIL_WINDOW_HANDLE>(widgetWindowId);
    qDebug() << "Widget Window Handle: " << widgetWindowHandle;
    MdispSelectWindow(m_display, m_buffer, widgetWindowHandle);

    MdigInquire(m_digitizer, M_SIZE_X,    &m_bufferSizeX);
    MdigInquire(m_digitizer, M_SIZE_Y,    &m_bufferSizeY);
    MdigInquire(m_digitizer, M_SIZE_BAND, &m_bufferSizeBand);
    MdigInquire(m_digitizer, M_TYPE, &m_bufferType);
    qDebug() << "Digitizer: " << m_bufferSizeX << m_bufferSizeY << m_bufferSizeBand << m_bufferType;

    int windowWidth = static_cast<int>(m_bufferSizeX);
    int windowHeight = static_cast<int>(m_bufferSizeY);
    emit windowResized(windowWidth, windowHeight);
}



void FrameGrabber::on_grab()
{
    if (!m_isInitialized) {
        qDebug() << "Error, frame grabber not initialized!";
        return;
    }

    if (!m_isGrabbingContinous) {
        MdigGrabContinuous(m_digitizer, m_buffer);
        m_isGrabbingContinous = true;
        qDebug() << "Stream is continous.";
    }

}


void FrameGrabber::on_capture()
{
    if (!m_isInitialized) {
        qDebug() << "Error, frame grabber not initialized!";
        return;
    }

    if (!m_isGrabbingContinous) {
        MdigGrab(m_digitizer, m_buffer);
        qDebug() << "Frame Captured.";
    }
}


void FrameGrabber::on_stop()
{
    if (!m_isInitialized) {
        qDebug() << "Error, frame grabber not initialized!";
        return;
    }

    if (m_isGrabbingContinous) {
        MdigHalt(m_digitizer);
        m_isGrabbingContinous = false;
        qDebug() << "Acquisition is stopped.";
    }
}
